const { EventHubClient } = require("@azure/event-hubs");

// Define connection string and the name of the Event Hub
const connectionString = "Endpoint=sb://sixtycentseventhub.servicebus.windows.net/;SharedAccessKeyName=sender;SharedAccessKey=ajEI1QTSetC8IiclBqhToLpakQpAo2IiPN0Qf5TIYJE=";
const eventHubsName = "sixtycentsevents";

async function main() {
  const client = EventHubClient.createFromConnectionString(connectionString, eventHubsName);

  for (let i = 0; i < 2; i++) {
    const eventData = {body: `Event ${i}`};
    console.log(`Sending message: ${eventData.body}`);
    await client.send(eventData);
  }

  await client.close();
}

main().catch(err => {
  console.log("Error occurred: ", err);
});