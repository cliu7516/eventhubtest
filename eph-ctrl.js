const { spawn } = require('child_process');

var cp;
function startProcess() {
    cp = spawn('node', ['./eph.js']);
    cp.stdout.on('data', (data) => {
        console.log(`${data}`);
      });
    cp.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
        startProcess();
    });
}

startProcess();
