var events = require('events');
const { EventProcessorHost, delay } = require("@azure/event-processor-host");

var eventEmitter = new events.EventEmitter();
var isEpnStarting = false;

// Define connection string and related Event Hubs entity name here
const eventHubConnectionString = "Endpoint=sb://sixtycentseventhub.servicebus.windows.net/;SharedAccessKeyName=sender;SharedAccessKey=ajEI1QTSetC8IiclBqhToLpakQpAo2IiPN0Qf5TIYJE=";
const eventHubName = "sixtycentsevents";
const storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=sixtycentsstorage;AccountKey=u2kUYsBO9YWu87TgVTbrs5fKjnBfrkarAfxcc05O67XayAAy5dTzSiKkhnzUFaXt4sNUzMB0OcyGNWhWKZLoMA==;EndpointSuffix=core.windows.net";

var eph;

var msgDict = {};

async function processMessage(message) {
    console.log(`Received message: ${message}`);
    var retry = 0;
    if (msgDict[message]) {
        retry = msgDict[message];
    }
    if (retry < 4) {
        msgDict[message] = ++retry;
        console.log(`Retry times: ${retry}`)
        throw new Error('Test');
    } else {
        delete msgDict[message];
    }
    await delay(1000)
    console.log(`Message: ${message} processed!`)
}

async function restartEph() {
  console.log('stopping.......')
  await eph.stop();
  delete eph
  console.log('stopped.......')
  await startEph();
  console.log('restarted.......')
}

var ephIndex = 0;

async function startEph() {
    if (isEpnStarting) {
        return;
    }
    isEpnStarting = true;
    console.log('starting EPH........')
    ephIndex++;
    eph = EventProcessorHost.createFromConnectionString(
        "localhost" + ephIndex,
        storageConnectionString,
        "sixtycentsblobstoragde",
        eventHubConnectionString,
        {
          eventHubPath: eventHubName,
          onEphError: (error) => {
            console.log("Found Error: %s", error);
          }
        }
      );
    
      await eph.start(async (context, eventData) => {
        // await processMessage(eventData.body);
        try {
            await processMessage(eventData.body);
        } catch (error) {
            console.log('test error!')
            // process.exit(1)
            setTimeout(restartEph, 200);
            return;      
        }
        await context.checkpoint()
      }, error => {
        console.log('Error when receiving message: %s', error)
      });
      isEpnStarting = false;
}

async function main() {
  startEph();

  // Sleep for a while before stopping the receive operation.
  await delay(1500000);
  await eph.stop();
}

main().catch(err => {
  console.log("Error occurred: ", err);
});
